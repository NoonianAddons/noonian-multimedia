function ($q) {
    

    
    
    var AudioPlayer = function(retainCache, allowOverlap) {
        if(retainCache) {
            this.bufferCache = {};
        }
        this.allowOverlap = allowOverlap;
        
        
    };
    
    
    
    AudioPlayer.prototype.getAudioContext = function() {
        if(!this._audioContext) {
            this._audioContext = new AudioContext();
        }
        return this._audioContext;
    };
    
    
    AudioPlayer.prototype.playBuffer = function(buffer) {
        
        if(!this.allowOverlap && this.currSrc) {
            this.currSrc.disconnect();
        }
        
        const ac = this.getAudioContext();
        var source = this.currSrc = ac.createBufferSource();
        source.buffer = buffer;
        source.connect(ac.destination);
        source.start(0);   
        
        // note: on older systems, may have to use deprecated noteOn(time);
        var deferred = $q.defer();
        source.onended  = ()=>{
            deferred.resolve(source);
            this.currSrc.disconnect();
            this.currSrc = null;
        };
        return deferred.promise;
    };
    
    
    AudioPlayer.prototype.playUrl = function(link) {
        var THIS = this;
        var cache = this.bufferCache;
        
        if(cache && cache[link]) {
            // console.log(link+' is buffered');
            return this.playBuffer(cache[link]);
        }
        
        var request = new XMLHttpRequest();
        request.open('GET', link, true);
        request.responseType = 'arraybuffer';
        
        var deferred = $q.defer();
        // Decode asynchronously
        request.onload = ()=>{
            // console.log('response for '+link, request.response.byteLength);
            
            this.getAudioContext().decodeAudioData(request.response, buffer=>{
                
                if(cache) {
                    cache[link] = buffer;
                }
                deferred.resolve(this.playBuffer(buffer));
            }, function(err) {
                console.error(err);
                deferred.reject(err);
            });
        }
        request.send();
        return deferred.promise;
    };
    
    AudioPlayer.prototype.stopPlayback = function() {
        this.currSrc && this.currSrc.disconnect(this.getAudioContext().destination);
    };
    
    
    return AudioPlayer;
}