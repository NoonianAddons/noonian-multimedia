function(db, _, Q) {
    const fs = require('fs');
    const ffmpeg = require('ffmpeg');
    const exports = {};
    
    
    
    const pairRegex = /^([^=]+)=([^=]+)$/;
    const sectionRegex = /^\[([^\]]+)\]$/;
    const parseMetadataFile = function(filePath) {
        var content = fs.readFileSync(filePath, 'UTF-8');
        content = content.split('\n');
        
        var result = {};
        var currSection = result;
        content.forEach(line=>{
            let pairCheck = pairRegex.exec(line);
            let titleCheck = sectionRegex.exec(line);
            
            if(pairCheck) {
                let k = pairCheck[1], v = pairCheck[2];
                currSection[k] = v;
            }
            else if(titleCheck) {
                let title = titleCheck[1];
                result[title] = result[title] || [];
                currSection = {};
                result[title].push(currSection);
            }
            
        });
        
        return result;
    };
    
    exports.getMetadata = function(filePath) {
        const process = new ffmpeg(filePath);
        return process.then(video=>video.metadata);
    }
    
    
    
    //metadata "TIMEBASE" value -> convert start/end times to seconds
    const timebaseRegex = /1\/(\d+)/;
    const getDivisor = timebase=>{
        let parsed = timebaseRegex.exec(timebase);
        let num = parsed && +parsed[1];
        
        if(!num  || isNaN(num)) {
            console.error('invalid TIMEBASE:'+timebase);
            return 0;
        } 
        
        return num;
    };
    
    //Convert seconds to HH:MM:SS
    const formatHMS = seconds=>{
        let hrs = Math.floor(seconds/3600);
        seconds -= hrs*3600;
        let min = Math.floor(seconds/60);
        seconds -= min*60;
        
        if(hrs < 10) {
            hrs = '0'+hrs;
        }
        if(min < 10) {
            min = '0'+min
        }
        if(seconds < 10) {
            seconds = '0'+seconds;
        }
        
        return hrs+':'+min+':'+seconds;
    };
    
    //Get metadata from file; return list of chapters/time offsets suitable for assigning to sketches field
    //  [{title, start_time, end_time}]
    exports.getChapters = function(filePath) {
        
        // filePath = filePath.replace(/ /g, '\\ ');
        const process = new ffmpeg(filePath);
        
        return process.then(video=>{
            
            // video.file_path = `"${video.file_path}"`;
            
            // console.log(video);
            
            // return null;
            
            video.addCommand('-c', 'copy');
            video.addCommand('-map_metadata', '0')
            video.addCommand('-map_metadata:s:v','0:s:v');
            video.addCommand('-map_metadata:s:a','0:s:a');
            video.addCommand('-f','ffmetadata');
            
            
            return video.save('/tmp/'+(new Date().getTime())+'.txt');
        },
        err=>console.error(err)
        )
        .then(textFilePath=>{
            
            if(!textFilePath) {
                return 'failed to save metadata';
            }
            const md = parseMetadataFile(textFilePath);
            
            fs.unlinkSync(textFilePath);
            
            const result = [];
            
            if(md) {
                _.forEach(md.CHAPTER, c=>{
                    let div = getDivisor(c.TIMEBASE);
                    let start = +c.START;
                    let end = +c.END;
                    
                    if(div && !isNaN(start+end)) {
                        start = Math.round(start/div);
                        end = Math.round(end/div);
                        
                        result.push({
                            title:c.title,
                            startSeconds:start,
                            endSeconds:end,
                            start:formatHMS(start),
                            end:formatHMS(end)
                        });
                    }
                    else {
                        console.error('bad chapter desc %j', c);
                    }
                });
            }
            
            
            return result;
            
        })
        ;
    };
    
    
    //formats:
    // attachment - loads image into gridfs; returns json for a BO "attachment" field
    // image - converts to value suiatable for "image" field
    // anything else - full path to the resulting jpeg on the filesystem (in /tmp);
    //time param conforms to ffmpeg time duration syntax: 
    //   https://ffmpeg.org/ffmpeg-utils.html#time-duration-syntax
    exports.getFrame = function(filePath, time, targetWidth, format) {
        // ffmpeg -ss 0.5 -i inputfile.mp4 -t 1 -s 480x300 -f image2 imagefile.jpg
        targetWidth = targetWidth || 250;
        
        var outputFile = (new Date().getTime())+'.jpg';
        var outputPath = '/tmp/'+outputFile;
        
        const scaleParam = 'scale='+targetWidth+':-1';
        const process = new ffmpeg(filePath);
        
        return process.then(video=>{
            video.addCommand('-ss', time);
            video.addCommand('-vframes', '1')
            video.addCommand('-vf',scaleParam);
            video.addCommand('-f','image2');
            
            return video.save(outputPath);
        })
        .then(jpgFile=>{
            
            if(!jpgFile) {
                console.error(process);
                return 'failed to extract image';
            }
            
            if(format === 'attachment') {
                return db._svc.GridFsService.importFile(inPath, 'image/jpeg').then(att=>{
                    fs.unlinkSync(jpgFile);
                    return att;
                });
            }
            else if(format === 'image') {
                var contents = fs.readFileSync(jpgFile);
                var encoded =  new Buffer(contents).toString('base64');
                var dataUrl = 'data:image/jpeg;base64,'+encoded;
                fs.unlinkSync(jpgFile);
                return {
                    name:outputFile,
                    type:'image/jpeg',
                    data:dataUrl,
                    thumbData:dataUrl
                }
            }
            else {
                return jpgFile;
            }
        });
    };
    
    /*
    ffmpeg -i input.avi -c:v libx264 -crf 21 -preset faster -pix_fmt yuv420p -maxrate 5000K -bufsize 5000K -vf 'scale=if(gte(iw\,ih)\,min(1920\,iw)\,-2):if(lt(iw\,ih)\,min(1920\,ih)\,-2)' -movflags +faststart -c:a aac -b:a 160k output.mp4
    */
    exports.transcodeForOdysee = function(inputPath, outputPath) {
        const process = new ffmpeg(inputPath);
        
        return process.then(video=>{
            try {
                video.setVideoCodec('h264');
                
                video.addCommand('-c:a', 'aac');
                video.addCommand('-b:a', '160k');
                
                video.addCommand('-crf', '21');
                video.addCommand('-preset', 'faster');
                video.addCommand('-pix_fmt', 'yuv420p');
                video.addCommand('-maxrate', '5000K');
                video.addCommand('-bufsize', '5000K');
                video.addCommand('-vf', '\'scale=if(gte(iw\\,ih)\\,min(1920\\,iw)\\,-2):if(lt(iw\\,ih)\\,min(1920\\,ih)\\,-2)\'');
                video.addCommand('-movflags', '+faststart');
                
                return video.save(`"${outputPath}"`).then(result=>{
                    console.log(result);
                    return result;
                });
            }  catch (e) {
            	console.error('Error running ffmpeg: '+e.code);
            	console.error(e.msg);
            }
        },
        err=>{
            console.error('Error processing '+inputPath);
            console.error(err.code);
        })
    };
    
    return exports;
}