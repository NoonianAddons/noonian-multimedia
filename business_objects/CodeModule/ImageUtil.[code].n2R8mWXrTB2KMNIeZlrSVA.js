function (db, DataUriUtil, Q, _) {
    
    const exports = {};
    
    const fs = require('fs');
    const path = require('path');
    
    const jimp = require('jimp');
    const mime = require('mime');
    
    const supportedTypes = {
        "image/png":true,
        "image/jpeg":true,
        "image/bmp":true
    };
    
    /*
    * @param image js object - value of fieldType "image"
    */
    exports.resizeImage = function(image, maxSize, maxThumbSize) {
    
        var myMime = image && image.type && mime.lookup(image.type);
        
        if(!supportedTypes[myMime]) {
            throw 'Unsupported image type: '+image.type;
        }
        
        //We're dealing with an image supported by jimp... scale it down and create a thumb
       
        const deferred = Q.defer();
        
        const imgData = image.data;
        const imgName = image.name;
        
        
        const scaleToMaxDim = function(jimpImg, MAX_SIZE) {
            var w = jimpImg.bitmap.width;
            var h = jimpImg.bitmap.height;
            
            var scaleFactor = 1;
            if(w > MAX_SIZE) {
                scaleFactor = MAX_SIZE/w;
            }
            
            if(h > MAX_SIZE) {
                scaleFactor = Math.min(scaleFactor, MAX_SIZE/h);
            }
            
            
            if(scaleFactor < 1) {
                jimpImg.scale(scaleFactor);
            }
        };
        
        
        var inBuff = DataUriUtil.uriToBuffer(imgData);
        jimp.read(inBuff).then(function(jimpImg) {
            
            scaleToMaxDim(img, maxSize);
            
            jimpImg.getBase64(jimp.AUTO, function(err, result) {
                
                if(err) {
                    console.error('ERROR processing image');
                    console.error(err);
                    deferred.reject(err);
                }
                else {
                    var newImageObj = {
                        data:result,
                        name:imgName,
                        type:img.getMIME()
                        
                    };
                    scaleToMaxDim(jimpImg, maxThumbSize);
                    jimpImg.getBase64(jimp.AUTO, function(err, result) {
                        if(err) {
                            console.error('ERROR processing image');
                            console.error(err);
                        }
                        else {
                            console.log('resized thumbdata');
                            newImageObj.thumbData = result;
                        }
                        deferred.resolve(newImageObj);
                    });
                }
            });
            
        }).catch(function(err) {
            console.error('ERROR reading image into inBuff');
            console.error(err);
            deferred.reject(err);
        });
        
        
        return deferred.promise;
    };
    
    
    const getFilename = function(imgObj) {
        if(imgObj.name) 
            return imgObj.name;
        
        let mimetype = imgObj.type;
        let ext = mimetype.substring(mimetype.indexOf('/')+1);
        return (new Date().getTime())+'.'+ext;
    };
    
    /**
     * utility to convert an image object (value for an "image" FieldType) into an attachment
     */
    exports.imageToAttachment = function(imgObj, filename) {
        let imgDataBuffer = DataUriUtil.uriToBuffer(imgObj.data);
        let imgFilePath = path.join('/tmp', filename || getFilename(imgObj));
        
        let ws = fs.createWriteStream(imgFilePath);
        ws.end(imgDataBuffer);
        
        let deferred = Q.defer();
        
        ws.on('error', err=>deferred.reject(err));
        
        ws.on('finish', ()=>deferred.resolve(
                db._svc.GridFsService.importFile(imgFilePath, imgObj.type).then(att=>{
                    fs.unlinkSync(imgFilePath);
                    return att;
                })
            )
        );
        
        return deferred.promise;
    };
    
    
    return exports;
}